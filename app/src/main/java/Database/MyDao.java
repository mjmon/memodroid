package Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


@Dao
public interface MyDao {

    @Insert
    public void addUser(Memo memo);

    @Query("select * from TableMemo ORDER BY id DESC")
    public List<Memo> getAllMemo();

    @Query("select * from TableMemo where id = :mID")
    public Memo getSpecificMemo(int mID);

    @Update
    public void updateMemo(Memo memo);

    @Delete
    public void deleteMemo(Memo memo);

}
