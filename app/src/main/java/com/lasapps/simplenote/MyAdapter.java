package com.lasapps.simplenote;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Database.Memo;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private final String TAG = "MyAdapter";

    List<Memo> memoList;
    Context context;


    public MyAdapter(List<Memo> memoList, Context context) {
        this.memoList = memoList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG, "onCreateViewHolder: ");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_memo, viewGroup, false);
        if(view==null) {
            return null;
        }
        MyViewHolder myViewHolder = new MyViewHolder(view, context);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder myViewHolder, final int i) {
        Log.d(TAG, "onBindViewHolder: ");

        myViewHolder.txtTitle.setText(memoList.get(i).getTitle());
        myViewHolder.txtDesc.setText(memoList.get(i).getDescription());
        myViewHolder.txtDate.setText(memoList.get(i).getDisplayDate());
        myViewHolder.txtTime.setText(memoList.get(i).getDisplayTime());

        myViewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddEditActivity.class);
                intent.putExtra("PASSED_ID", memoList.get(i).getId()); //pass memo id then fetch databse info of that id
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return memoList.size();
    }

    public void updateList(List<Memo> newList) {
        memoList = new ArrayList<>();
        memoList.addAll(newList);
        notifyDataSetChanged();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private static final String TAG = "MyViewHolder";

        TextView txtTitle, txtDesc, txtDate, txtTime;
        ConstraintLayout container;
        Context mContext;

        public MyViewHolder(@NonNull View itemView, Context mContext) {
            super(itemView);
            Log.d(TAG, "MyViewHolder: ");
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDesc = itemView.findViewById(R.id.txtDesc);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtTime = itemView.findViewById(R.id.txtTime);
            container = itemView.findViewById(R.id.container);

            this.mContext = mContext;
        }

        public void bind(){
        }
    }


}
