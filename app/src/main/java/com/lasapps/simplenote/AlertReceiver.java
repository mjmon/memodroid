package com.lasapps.simplenote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

public class AlertReceiver extends BroadcastReceiver {



    public AlertReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getBundleExtra("KEY");

        if(bundle!=null){
            String title = bundle.getString("TITLE");
            String desc = bundle.getString("DESC");
            int id = bundle.getInt("ID");

            NotificationHelper notificationHelper = new NotificationHelper(context, title, desc);
            NotificationCompat.Builder nb = notificationHelper.getChannelNotification();
            notificationHelper.getManager().notify(id, nb.build());
        }
    }
}
