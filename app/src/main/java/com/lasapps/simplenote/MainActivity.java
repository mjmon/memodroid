package com.lasapps.simplenote;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;

import Database.Memo;
import Database.MyAppDatabase;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {

    private final String TAG = "MainActivity";
    
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton fab;
    private AdView adView;
    private AdRequest adRequest;
    private List<Memo> memoList;

    private static boolean adFlag = false;

    public static MyAppDatabase myAppDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: ");

        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "memodb")
                .allowMainThreadQueries()
                .build();

        MobileAds.initialize(this, getString(R.string.app_id));
        adView = findViewById(R.id.adView);

        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                adView.setVisibility(View.VISIBLE); //show if adView Container
                adFlag = true; //ad loaded successfully
            }

            @Override
            public void onAdFailedToLoad(int i) {
               if(adFlag) { //ad was loaded once successfully and later it failed
                   adView.setVisibility(View.GONE);
               }
            }
        });

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);


        mRecyclerView = findViewById(R.id.myRecyclerview);
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

//        handleIntent(getIntent());
    }


    @Override
    protected void onResume() { //load ads upon resume as well
        super.onResume();
        adView.loadAd(adRequest);

        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                adView.setVisibility(View.VISIBLE); //show if adView Container
                adFlag = true; //ad loaded successfully
            }

            @Override
            public void onAdFailedToLoad(int i) {
                if(adFlag) { //ad was loaded once successfully and later it failed
                    adView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: ");
        
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d(TAG, "onPostResume: ");
        
        memoList = myAppDatabase.myDao().getAllMemo();
        mAdapter = new MyAdapter(memoList, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void displayAddEditActivity() {
        Log.d(TAG, "displayAddEditActivity: ");
        
        Intent intent = new Intent(this, AddEditActivity.class);
        intent.putExtra("KEY", true); //true for Adding
        startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: ");
        
        switch (v.getId()){
            case R.id.fab:
                displayAddEditActivity();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        Log.d(TAG, "onQueryTextChange: " + s);

        String userInput = s.toLowerCase();
        List<Memo> newList = new ArrayList<>();

        for(Memo memo : memoList) {
            if(memo.getTitle().toLowerCase().contains(userInput)){
                newList.add(memo);
            }
        }

        mAdapter.updateList(newList);
        return true;
    }
}
