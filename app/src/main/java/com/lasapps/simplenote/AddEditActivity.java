package com.lasapps.simplenote;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import Database.Memo;

public class AddEditActivity extends AppCompatActivity implements View.OnClickListener{
    private final String TAG = "AddEditActivity";

    private ActionBar actionBar;
    private EditText title, desc;
    private TextView date, time;
    private Button save, delete;
    private int day, month, year;
    private Calendar currentDate;
    private Boolean flag = true;
    private Memo passedMemo;

    private int selectedMonth, selectedDay, selectedYear;
    private int selectedHour, selectedMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        Log.d(TAG, "onCreate: ");

        setupViews();
        initializeCurrentDateValues();
    }

    private void initializeCurrentDateValues() {
        currentDate = Calendar.getInstance();
        day = currentDate.get(Calendar.DAY_OF_MONTH);
        month = currentDate.get(Calendar.MONTH);
        year = currentDate.get(Calendar.YEAR);

        month++;

//        selectedDay = passedMemo.getSelectedDay();
//        selectedMonth = passedMemo.getSelectedMonth();
//        selectedYear = passedMemo.getSelectedYear();
//        selectedHour = 0;
//        selectedMinute = 0;

    }

    private void setupViews() {
        Log.d(TAG, "setupViews: ");
        
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        title = findViewById(R.id.title);
        desc = findViewById(R.id.desc);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        save = findViewById(R.id.save);
        delete = findViewById(R.id.delete);

        date.setOnClickListener(this);
        time.setOnClickListener(this);
        save.setOnClickListener(this);
        delete.setOnClickListener(this);

        flag = getIntent().getBooleanExtra("KEY", false);
        String barTitle = (flag)? getString(R.string.addNote): getString(R.string.editNote);
        actionBar.setTitle(barTitle);

        if(!flag) {//perform edit and set inital values from the database
            int mID = getIntent().getIntExtra("PASSED_ID", -1);
            passedMemo = MainActivity.myAppDatabase.myDao().getSpecificMemo(mID);
            title.setText(passedMemo.getTitle());
            desc.setText(passedMemo.getDescription());
            date.setText(passedMemo.getDisplayDate());
            time.setText(passedMemo.getDisplayTime());
            save.setText("UPDATE");
            delete.setVisibility(View.VISIBLE);

            selectedDay = passedMemo.getSelectedDay();
            selectedMonth = passedMemo.getSelectedMonth();
            selectedYear = passedMemo.getSelectedYear();
            selectedHour = 0;
            selectedMinute = 0;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Log.d(TAG, "onSupportNavigateUp: ");
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: ");
        switch (v.getId()){
            case R.id.date://trigger date dialog
                displayDatePickerDialog();
                break;
            case R.id.time://trigger time dialog
                displayTimePickerDialog();
                break;
            case R.id.save://save memo
                if(checkEntries()){
                    if(flag){ //perform save
                        saveToDatabase();
                    }
                    else { //perform update
                        updateSpecificMemo();
                    }
                }
                else{
                    toast("Please fill out all information");
                }
                break;
            case R.id.delete: //show dialog to confirm delete
                showDeleteConfirmation();
                break;
        }
    }


    private void showDeleteConfirmation() {
        Log.d(TAG, "showConfirmation: ");
        
        AlertDialog.Builder builder = new AlertDialog.Builder(AddEditActivity.this);
        builder.setTitle("Delete Confirmation");
        builder.setMessage("Are you sure you want to proceed");
        builder.setNegativeButton("No", null);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelAlarm(passedMemo.getRequestCode());
                MainActivity.myAppDatabase.myDao().deleteMemo(passedMemo);
                toast("Successfully deleted");
                finish();
            }
        });

        if(!(isFinishing())){
            builder.show();
        }

    }


    private void updateSpecificMemo() {
        String memoTitle = title.getText().toString().trim();
        String memoDesc = desc.getText().toString().trim();
        String memoDate = date.getText().toString();
        String memoTime = time.getText().toString();

        passedMemo.setId(passedMemo.getId());
        passedMemo.setTitle(memoTitle);
        passedMemo.setDescription(memoDesc);
        passedMemo.setDisplayDate(memoDate);
        passedMemo.setDisplayTime(memoTime);

        passedMemo.setSelectedMonth(selectedMonth);
        passedMemo.setSelectedDay(selectedDay);
        passedMemo.setSelectedYear(selectedYear);
        passedMemo.setSelectedHour(selectedHour);
        passedMemo.setSelectedMinute(selectedMinute);

        //add to alarm manager
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH - 1, selectedMonth);
        c.set(Calendar.DAY_OF_MONTH, selectedDay);
        c.set(Calendar.YEAR, selectedYear);

        c.set(Calendar.HOUR_OF_DAY, selectedHour);
        c.set(Calendar.MINUTE, selectedMinute);
        c.set(Calendar.SECOND, 0);

        startAlarm(c, passedMemo);

        MainActivity.myAppDatabase.myDao().updateMemo(passedMemo);
        Log.d(TAG, "onClick: forupdate " + passedMemo.getId() +"/"+ passedMemo.getTitle());
        toast("Successfully Updated");
    }


    private void saveToDatabase() {
        String memoTitle = title.getText().toString().trim();
        String memoDesc = desc.getText().toString().trim();
        String memoDate = date.getText().toString();
        String memoTime = time.getText().toString();

        Memo memo = new Memo();
        memo.setTitle(memoTitle);
        memo.setDescription(memoDesc);
        memo.setDisplayDate(memoDate);
        memo.setDisplayTime(memoTime);

        memo.setSelectedMonth(selectedMonth);
        memo.setSelectedDay(selectedDay);
        memo.setSelectedYear(selectedYear);
        memo.setSelectedHour(selectedHour);
        memo.setSelectedMinute(selectedMinute);

        int requestCode = Utils.generateRandomNumber();
        memo.setRequestCode(requestCode);
        //add to alarm manager
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH - 1, selectedMonth);
        c.set(Calendar.DAY_OF_MONTH, selectedDay);
        c.set(Calendar.YEAR, selectedYear);

        c.set(Calendar.HOUR_OF_DAY, selectedHour);
        c.set(Calendar.MINUTE, selectedMinute);
        c.set(Calendar.SECOND, 0);

        startAlarm(c, memo);

        MainActivity.myAppDatabase.myDao().addUser(memo);
        toast("Successfully Saved");
        clearEntries();
    }

    private void startAlarm(Calendar c, Memo memo) {

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);

        Bundle bundle = new Bundle();
        bundle.putString("TITLE", memo.getTitle());
        bundle.putString("DESC", memo.getDescription());
        bundle.putInt("ID", memo.getId());

        intent.putExtra("KEY", bundle);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, memo.getId(), intent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(c.after(Calendar.getInstance())) //add alarm if only set infos is for the future
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
        }
    }

    private void cancelAlarm(int mRequestCode) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, mRequestCode, intent, 0);

        alarmManager.cancel(pendingIntent);
    }



    private void clearEntries() {
        title.setText("");
        desc.setText("");
        date.setText("Select Date");
        time.setText("Select Time");
    }

    private void displayTimePickerDialog() {
        Log.d(TAG, "displayTimePickerDialog: ");
        Calendar c = Calendar.getInstance();
        int currentHour = c.get(Calendar.HOUR_OF_DAY);
        int currentMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEditActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        selectedHour = hourOfDay;
                        selectedMinute = minute;

                        String AmPm = (hourOfDay >= 12) ? "PM" : "AM";
                        hourOfDay = (hourOfDay > 12) ? (hourOfDay%12): hourOfDay;
                        if(hourOfDay == 0) {  //if 0. set to 12
                            hourOfDay = 12;
                        }
                        String completeTime = String.format("%02d:%02d", hourOfDay, minute) + AmPm;

                        time.setText(completeTime);
                    }
                },currentHour,currentMinute,false);
        timePickerDialog.show();
    }

    private void displayDatePickerDialog() {
        Log.d(TAG, "displayDatePickerDialog: ");

        Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH);
        int currentDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AddEditActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month++;

                        selectedMonth = month;
                        selectedDay = dayOfMonth;
                        selectedYear = year;

                        String selectedDate = month + "/" + dayOfMonth+ "/" + year;
                        SimpleDateFormat format1=new SimpleDateFormat("MM/dd/yyyy");
                        Date dt1= null;
                        try {
                            dt1 = format1.parse(selectedDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        DateFormat format2=new SimpleDateFormat("EEEE"); //for day name
                        DateFormat format3 = new SimpleDateFormat("MMM");

                        String dayName = format2.format(dt1);
                        String monthName = format3.format(dt1);
                        String completeDate = dayName + ", " + monthName + " " + dayOfMonth + ", " + year;

//                        actualDate = selectedDate; //1/1/1900

                        date.setText(completeDate);
                    }
                }, currentYear, currentMonth, currentDay);
        datePickerDialog.show();
    }



    private boolean checkEntries() { //checking if entries are not blank
        if(title.getText().toString().trim().isEmpty() ||
            desc.getText().toString().trim().isEmpty() ||
            date.getText().toString().trim().equals("Select Date") ||
            time.getText().toString().trim().equals("Select Time")) {
            return false;
        }
        return true; //all entries filled
    }


    private void toast(String myString){
        Toast.makeText(getApplicationContext(), myString, Toast.LENGTH_SHORT).show();
    }
}
